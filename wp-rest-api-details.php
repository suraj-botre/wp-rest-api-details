<?php
/**
 * Plugin Name: Wp Get Details Using Rest Api
 * Plugin URI: 
 * Description: Get Details From Any Third Party Rest Api
 * Version: 1.0.1
 * Author: Suraj Botre
 * Author URI: 
 * License: 
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

define('BASE_PATH', plugin_dir_path(__FILE__));

require BASE_PATH . 'vendor/autoload.php';

use Suraj\WPRestApiDetails\Plugin;

$plugin = new Plugin();
