<?php
$wp_rest_api_url = "";
$wp_rest_api_endpoint = "";
$wp_rest_api_column_names = "id,name,email,phone,username,website,address,company";

if(!empty($_POST['wp_rest_api'])) {

	$wp_rest_api_url = $_POST['wp_rest_api']['url'];
	$wp_rest_api_endpoint = $_POST['wp_rest_api']['endpoint'];
	$wp_rest_api_column_names = $_POST['wp_rest_api']['column_names'];

	update_option('wp_rest_api', $_POST['wp_rest_api']);

} else {

	$wp_rest_api = get_option('wp_rest_api');
	if(!empty($wp_rest_api)) {

		$wp_rest_api_url = $wp_rest_api["url"];
		$wp_rest_api_endpoint = $wp_rest_api["endpoint"];
		$wp_rest_api_column_names = $wp_rest_api['column_names'];

	}

}

$column_names = explode(",", $wp_rest_api_column_names);
?>

<html>
	<body>
		<form action="" method="POST">
			<table>
				<tr>
					<td>WP Rest Api URL (https://example.com)</td>
					<td><input class="regular-text" type="text" name="wp_rest_api[url]" value="<?php echo $wp_rest_api_url; ?>"></td>
				</tr>
				<tr>
					<td>WP Rest Api Endpoint (users)</td>
					<td><input class="regular-text" type="text" name="wp_rest_api[endpoint]" value="<?php echo $wp_rest_api_endpoint; ?>"></td>
				</tr>
				<tr>
					<td>Enter Column Names (Comma(,) Seperated List)</td>
					<td><input class="regular-text" type="text" name="wp_rest_api[column_names]" value="<?php echo $wp_rest_api_column_names; ?>"></td>
				</tr>
			</table>
			<p>
				<input type="submit" value="Save">
			</p>
		</form>
		<p>
			<a id="show-all" href="">Show All</a>
		</p>
		<table id="wp-rest-api-user-details" border="1"></table>
	</body>
</html>

<script type="text/javascript">
var url = "<?php echo $wp_rest_api_url; ?>";
if(url) url += "/"+"<?php echo $wp_rest_api_endpoint; ?>";

jQuery(document).ready(function() {

	jQuery("#show-all").val(url);

	if(url) {

		getEndpointData(url);
	
	}

	jQuery(document).on("click", "#get-details-by-id", function(e) {

		e.preventDefault();
		getEndpointData(url, jQuery(this).data("id"));

	});

});

function getEndpointData(url, id = 0) {

	var user_details = '';

	if(id) url += "/"+id;

	jQuery.ajax({

		url: url,
		success: function(user_data) {

			if(id) {

				user_data = [user_data];
			
			}

			jQuery.each( user_data, function( key, user ) {
			
				user_details = getUserDetails(user);
				
				if(id) jQuery("#wp-rest-api-user-details").html(user_details);
				else jQuery("#wp-rest-api-user-details").append(user_details);
			
			});

		}

	});

}

function getUserDetails(user) {

	var column_names = <?php echo json_encode($column_names) ?>;
	var column_name = '';
	var user_details = '';

	for (var i = 0; i < column_names.length; i++) {
		
		user_details += "<td><a id='get-details-by-id' data-id='"+user['id']+"' href='"+url+"/"+user['id']+"'>"+JSON.stringify(user[column_names[i]])+"</a></td>";
	
	}

	user_details = "<tr>"+user_details+"</tr>";
	
	return user_details;

}
</script>
