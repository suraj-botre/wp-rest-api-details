<?php
/**
 * This is the class for this plugin.
 */

namespace Suraj\WPRestApiDetails;

class Plugin
{

    public function __construct() {

        add_action( 'admin_menu', array($this, 'custom_admin_menu') );

    }

    public function custom_admin_menu() {

        add_menu_page( 'WP Rest API Details', 'WP Rest API Details', 'manage_options', 'wp-rest-api-details', array( $this, 'wp_rest_api_details_callback' ) );

    }

    public function wp_rest_api_details_callback() {

        include(BASE_PATH . 'inc/wp-rest-api-details-page.php');

    }

}
