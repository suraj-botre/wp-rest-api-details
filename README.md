What is it?

WP Plugin to get details using rest api.

Installation :

Add below details in composer.json

{
	"repositories":[
		{
			"type": "vcs",
			"url": "https://bitbucket.org/suraj-botre/wp-rest-api-details"
		}
	],
	"require": {
		"suraj-botre/wp-rest-api-details": "master@dev"
	}
}

After receiving plugin using command "composer install / composer update", run same command in plugin folder "wp-rest-api-details" to update plugin dependencies.